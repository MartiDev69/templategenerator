package core.enumeration;

public enum CheminFixe {

	CONFIG_XML("src/main/java/configuration/config.xml");

	public String getChemin() {
		return chemin;
	}

	public void setChemin(String chemin) {
		this.chemin = chemin;
	}

	String chemin;

	CheminFixe(String chemin) {
		this.chemin = chemin;
	}

}
