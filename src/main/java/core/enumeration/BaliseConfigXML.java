package core.enumeration;

public enum BaliseConfigXML {
	DIRECTORY("directory"),DIRECTORY_TEMPLATE("template"),VARIABLE("variable");

	public String getNomBalise() {
		return nomBalise;
	}

	public void setNomBalise(String nomBalise) {
		this.nomBalise = nomBalise;
	}

	String nomBalise;

	BaliseConfigXML(String nomBalise) {
		this.nomBalise = nomBalise;
	}
}
