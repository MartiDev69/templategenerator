package core.xml;

import java.io.File;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import core.enumeration.BaliseConfigXML;
import core.enumeration.CheminFixe;

public class XMLInformation {

	private static Logger logger;

	private static XMLInformation instance;

	private Document doc = null;
	private DocumentBuilder dBuilder = null;

	// information du fichier xml
	private String directory;
	
	private String directoryTemplate;

	private XMLInformation() {

		// Charger le fichier de configuration XML
		File configFile = new File(CheminFixe.CONFIG_XML.getChemin());
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

		try {
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(configFile);
		} catch (Exception e) {
			logger.log(Level.ERROR, e);
		}
		this.chargementInformationXML(doc);

	}

	public static XMLInformation getInstance() {
		if (instance == null) {
			instance = new XMLInformation();
		}
		return instance;
	}

	private void chargementInformationXML(Document doc) {
		directory = doc.getElementsByTagName(BaliseConfigXML.DIRECTORY.getNomBalise()).item(0).getTextContent();
		directoryTemplate = doc.getElementsByTagName(BaliseConfigXML.DIRECTORY_TEMPLATE.getNomBalise()).item(0).getTextContent();
	}
	
	public NodeList recuperationInformationVariable() {
		return doc.getElementsByTagName(BaliseConfigXML.VARIABLE.getNomBalise());
	}

	public String getDirectory() {
		return directory;
	}
	
	public String getDirectoryTemplate() {
		return directoryTemplate;
	}



}
