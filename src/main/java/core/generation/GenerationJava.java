package core.generation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import core.mustache.DataMustache;
import core.xml.XMLInformation;

public class GenerationJava {
	private String cheminGeneration;
	private List<String> listAbsolutePathMustache = new ArrayList<>();
	private List<String> listPathMustache = new ArrayList<>();
	private Map<String, Object> data = new HashMap<>();
	private DataMustache dataXmlVariable = new DataMustache();

	public GenerationJava(List<String> listAbsolutePathMustache, List<String> listPathMustache) {
		this.listAbsolutePathMustache = listAbsolutePathMustache;
		this.listPathMustache = listPathMustache;
		cheminGeneration = XMLInformation.getInstance().getDirectory();
		dataXmlVariable = new DataMustache();
		data = dataXmlVariable.getData();
	}

	public void createClasse() throws IOException {

		for (int i = 0; i < listAbsolutePathMustache.size(); i++) {

			Map<String, Object> dataExecute = this.data;

			// Création des chemins
			String classeMustache = listAbsolutePathMustache.get(i);
			String classJava = cheminGeneration + "\\" + listPathMustache.get(i).replace(".mustache", ".java");
			String cheminPackage = this.removeLastFichier(listPathMustache.get(i)).replaceFirst("\\\\", "").replaceAll("\\\\", ".");
			String classDirectory = this.removeLastFichier(classJava);

			ClassJavaGenerate classJavaGenerate = new ClassJavaGenerate(classeMustache, classJava, classDirectory,
					dataExecute,cheminPackage);
			classJavaGenerate.generation();

		}

	}

	private String removeLastFichier(String chemin) {
		// Trouver l'emplacement du dernier caractère séparateur de chemin
		int lastIndex = chemin.lastIndexOf(File.separator);
		// Extraire le chemin sans le nom de fichier
		String newPath = chemin.substring(0, lastIndex);
		return newPath;
	}
}
