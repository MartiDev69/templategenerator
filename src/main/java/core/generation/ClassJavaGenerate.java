package core.generation;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

import core.enumeration.FixeString;

public class ClassJavaGenerate {
	public ClassJavaGenerate(String classeMustache, String classJava, String classDirectory, Map<String, Object> data,
			String cheminPackage) {
		super();
		this.classeMustache = classeMustache;
		this.classJava = classJava;
		this.classDirectory = classDirectory;
		this.data = data;
		this.mf = new DefaultMustacheFactory();
		this.cheminPackage = cheminPackage;
	}

	private String classeMustache;
	private String classJava;
	private String classDirectory;
	private Map<String, Object> data;
	private MustacheFactory mf;
	private String cheminPackage;

	public void generation() throws IOException {
		FileReader file = new FileReader(new File(classeMustache));
		Mustache mustache = mf.compile(file, "test.mustache");
		File fileJava = new File(classJava);
		File fileDirectory = new File(classDirectory);
		this.creationDossierIfNotExiste(fileDirectory);
		this.addDataClasse(data);
		Writer writer = new FileWriter(fileJava);
		mustache.execute(writer, data);
		writer.close();
	}

	private void creationDossierIfNotExiste(File chemin) {
		if (!chemin.exists()) {
			chemin.mkdirs();
		}
	}

	private void addDataClasse(Map<String, Object> data) {
		data.put(FixeString.PACKAGE_NAME.getNom(), cheminPackage);
	}
}
