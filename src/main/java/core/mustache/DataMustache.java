package core.mustache;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import core.xml.XMLInformation;

public class DataMustache {

	Map<String, Object> data = new HashMap<>();

	public DataMustache() {
		// data.put("packageName", "com.example.myapp");
		// data.put("imports", Arrays.asList("java.util.List", "java.util.Map"));

		// Écrire le résultat dans le fichier de sortie spécifié dans le fichier de
		// configuration XML
		// variable
		NodeList variableXML = XMLInformation.getInstance().recuperationInformationVariable();

		for (int i = 0; i < variableXML.getLength(); i++) {
			Node variableNode = variableXML.item(i);
			for (int j = 0; j < variableNode.getChildNodes().getLength(); j++) {

				Node currentNode = variableNode.getChildNodes().item(j);
				if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
					String contentVariable = currentNode.getTextContent();
					String localName = currentNode.getNodeName();
					data.put(localName, contentVariable);
				}
			}
		}
	}

	public Map<String, Object> getData() {
		return data;
	}
	
	

}
