package core.mustache;

import java.io.File;
import java.util.ArrayList;

import core.enumeration.FixeString;
import core.xml.XMLInformation;

public class MustachePathStorage {

	private ArrayList<String> listAbsolutePathMustache = new ArrayList<>();
	private ArrayList<String> listPathMustache = new ArrayList<>();

	public MustachePathStorage() {
		String directoryPath = XMLInformation.getInstance().getDirectoryTemplate();
		File directory = new File(directoryPath);
		if (directory.exists() && directory.isDirectory()) {
			File[] files = directory.listFiles();
			searchDirectoryRecursif(files);
		} else {
			System.out.println("Invalid directory path!");
		}
		for (String absolutePath : listAbsolutePathMustache) {
			String chemin = absolutePath;
			String directoryTemplace = XMLInformation.getInstance().getDirectoryTemplate();
			listPathMustache.add(chemin.replace(directoryTemplace, ""));
		}

		System.out.println(listPathMustache.toString());

	}

	private String getExtention(File fichier) {
		String fileName = fichier.getName();
		int lastIndex = fileName.lastIndexOf(".");
		if (lastIndex != -1 && lastIndex < fileName.length() - 1) {
			String extension = fileName.substring(lastIndex + 1);
			return extension;
		} else {
			return "";
		}
	}

	private void searchRecursif(File file) {
		if (file.isFile()) {
			if (getExtention(file).equals(FixeString.MUSTACHE.getNom())) {
				listAbsolutePathMustache.add(file.getAbsolutePath());
			}
		} else if (file.isDirectory()) {
			searchDirectoryRecursif(file.listFiles());
		}
	}

	private void searchDirectoryRecursif(File[] files) {
		if (files != null) {
			for (File file : files) {
				searchRecursif(file);
			}
		}
	}

	public ArrayList<String> getListAbsolutePathMustache() {
		return listAbsolutePathMustache;
	}

	public ArrayList<String> getListPathMustache() {
		return listPathMustache;
	}
}