package core;

import core.generation.GenerationJava;
import core.mustache.MustachePathStorage;

public class Main {

	public static void main(String[] args) throws Exception {

		MustachePathStorage storePath = new MustachePathStorage();
		GenerationJava generationClasse = new GenerationJava(storePath.getListAbsolutePathMustache(),
				storePath.getListPathMustache());
		generationClasse.createClasse();

	}
}
